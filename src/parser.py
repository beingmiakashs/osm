from lxml import etree
# import shapefile
from shapely.geometry import MultiPolygon, Polygon, Point, MultiPoint

def parse_poly(lines):

    coords = []

    for (index, line) in enumerate(lines):

        if index == 0 or index == 1:
            # junk lines.
            continue

        elif line.strip() == 'END':
            break

        else:
            # we are in a ring and picking up new coordinates.
            temp = line.split()
            lat = float(temp[0])
            lon = float(temp[1])
            coords.append((lon, lat))

    return MultiPoint(coords).convex_hull


content = []
with open('bangladesh.poly', 'r') as content_file:
    for line in content_file:
        content.append(line)
bangladeshShape = parse_poly(content)
print(bangladeshShape)
tempPoint = Point(21.212312, 92.164403)
print(bangladeshShape.contains(tempPoint))

resultRoot = etree.Element('osm')

fileName = '/media/akashs/Parents/OSM/discussions-latest.osm'
context = etree.iterparse(fileName, events=('end',), tag='changeset')
for event, elem in context:
    containFlag = False
    if ('min_lat' in elem.attrib) and ('min_lon' in elem.attrib):
        tempPoint = Point(float(elem.attrib['min_lon']), float(elem.attrib['min_lat']))
        if(bangladeshShape.contains(tempPoint)):
            containFlag=True

    if ('max_lat' in elem.attrib) and ('max_lon' in elem.attrib):
        tempPoint = Point(float(elem.attrib['max_lon']), float(elem.attrib['max_lat']))
        if(bangladeshShape.contains(tempPoint)):
            containFlag=True

    if containFlag:
        print(elem.attrib['id'])
        print(type(elem))
        resultRoot.append(elem)

with open('bangladeshChanngeSets.xml', 'wb') as f:
    f.write(etree.tostring(resultRoot))
